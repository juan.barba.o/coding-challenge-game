<?php


namespace Ucc\Controllers;


use Ucc\Services\QuestionService;
use Ucc\Session;

class QuestionsController extends Controller
{
    private QuestionService $questionService;

    public function __construct(QuestionService $questionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
    }

    public function beginGame(): bool
    {
        $name = $this->requestBody->name ?? null;
        if (empty($name)) {
            return $this->json('You must provide a name', 400);
        }

        Session::set('name', $name);
        Session::set('questionCount', 1);
        $question = $this->questionService->getRandomQuestions(1)[0];

        return $this->json(['question' => $question], 201);
    }

    public function answerQuestion(int $id): bool {
        if ( Session::get('name') === null ) {
            return $this->json('You must first begin a game', 400);
        }

        $question = $this->questionService->getQuestion($id);
        if (!$question) {
            return $this->json('Question not found', 404);
        }

        $questions_count = (int)Session::get('questionCount');
        $points = Session::get('points');
        if ($questions_count > 4) {
            $name = Session::get('name');
            Session::destroy();
            return $this->json(['message' => "Thank you for playing {$name}. Your total score was: {$points} points!"]);
        }

        $answer = $this->requestBody->answer ?? null;
        if (empty($answer)) {
            return $this->json('You must provide an answer', 400);
        }

        $points = $this->questionService->getPointsForAnswer($id, $answer);
        if ($points == 0) {
            $message = 'The answer was not correct';
        } else {
            $message = 'Correct answer!';
            Session::set('questionCount', $questions_count + 1);
            Session::set('points', $points + 1);
        }

        return $this->json(['message' => $message, 'question' => $question]);
    }
}