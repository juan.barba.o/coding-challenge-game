<?php


namespace Ucc\Services;


use JsonMapper;
use KHerGe\JSON\JSON;
use Ucc\Models\Question;
use Ucc\Session;

class QuestionService
{
    const QUESTIONS_PATH = __DIR__ . '/../../questions.json';

    private JSON $json;
    private JsonMapper $jsonMapper;
    /**
     * @var Ucc\Models\Question[] $questions
     */
    private array $questions;

    public function __construct(JSON $json, JsonMapper $jsonMapper)
    {
        $this->json = $json;
        $this->jsonMapper = $jsonMapper;
        $this->loadQuestions();
    }

    public function loadQuestions()
    {
        $questions = Session::get('questions');
        if (!$questions) {
            $questions_raw = $this->json->decode(file_get_contents(QuestionService::QUESTIONS_PATH));
            $questions = $this->jsonMapper->mapArray(
                $questions_raw, array(), new Question
            );
            Session::set('questions', serialize($questions));
        } else {
            $questions = unserialize($questions);
        }
        $this->questions = $questions;
    }

    /**
     * @return Question[]
     */
    public function getRandomQuestions(int $count = 5): array
    {
        $return = [];
        for ($i = 0; $i < $count; $i++) {
            $return[] = $this->questions[rand(0, count($this->questions) - 1)];
        }
        return $return;
    }

    /**
     * @return Question
     */
    public function getQuestion($question_id)
    {
        $question = reset(array_filter($this->questions, function($question) use ($question_id) {
            return $question->getId() == $question_id;
        }));
        return $question;
    }

    public function getPointsForAnswer(int $id, string $answer): int
    {
        $question = $this->getQuestion($id);
        if ($answer != $question->getCorrectAnswer()) {
            return 0;
        }
        return $question->getPoints();
    }
}